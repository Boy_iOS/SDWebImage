//
//  main.m
//  SDWebImage
//
//  Created by 赵国腾 on 15/9/5.
//  Copyright (c) 2015年 赵国腾. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
